Assiff Law is a Personal Injury Law Firm located in Edmonton, Alberta. We're dedicated and committed to the practice of personal injury litigation. If you or a loved one are injured in a car accident due to someone else’s negligence, call us immediately. We employ the best injury lawyers in Alberta.

Address: 10612 124 St, #300, Edmonton, AB T5N 1S4, Canada

Phone: 587-524-3000

Website: https://assifflaw.com
